import { RouterModule, Routes } from '@angular/router';
import { NavbarComponent } from "./components/shared/navbar/navbar.component";
const app_routes: Routes = [
    {path: 'navbar', component: NavbarComponent}
];

export const app_routing = RouterModule.forRoot(app_routes);