import { Component, OnInit,HostListener } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  otherNav: boolean = false;
  exit = true;
  sidenav = false;
  times = false;
  enter:Boolean;
  start = true;
  @HostListener('window:scroll', ['$event']) 
  scrollHandler(event) {
    
    if(window.pageYOffset > 70){
      
      this.otherNav = true;
    }else{
      
      this.otherNav = false;
    }
  }
  constructor() { }

  ngOnInit() {
  }

  menu(){
    this.sidenav = true;
    if(this.enter){
      this.times = true;
      this.exit = true;
      this.enter = false;
    }else{
      this.times = false;
      this.enter = true;
      this.exit = false;
    };
  }

}
